'use strict';
module.exports = (sequelize, DataTypes) => {
  const Votos = sequelize.define('Votos', {
    nome: DataTypes.STRING,
    total_votos: DataTypes.INTEGER
  }, {});
  Votos.associate = function(models) {
    // associations can be defined here
  };
  return Votos;
};