'use strict';
module.exports = (sequelize, DataTypes) => {
  const Usuarios = sequelize.define('Usuarios', {
    id_twitter: DataTypes.STRING,
    nome: DataTypes.STRING,
    url_foto: DataTypes.STRING,
    javotou: DataTypes.INTEGER
  }, {});
  Usuarios.associate = function(models) {
    // associations can be defined here
  };
  return Usuarios;
};