const express = require('express')
const cookieSession = require("cookie-session")
const cookieParser = require("cookie-parser")
const dotenv = require('dotenv').config()
const mustacheExpress = require('mustache-express')
const path = require('path')
const templateDir = path.join(__dirname, 'views')

const passport = require('passport')
const TwitterStrategy = require('passport-twitter').Strategy;

const { Usuarios } = require('./models');
const { Votos } = require('./models');

const app = express()
app.engine('mustache', mustacheExpress())
app.set('view engine', 'mustache')
app.set('views', templateDir)
app.use(express.static(__dirname + '/node_modules/bootstrap/dist'))
app.use(express.static(__dirname + '/views/static'))

app.use(
    cookieSession({
        name: "session",
        keys: [process.env.COOKIE_KEY],
        maxAge: 24 * 60 * 60 * 100
    })
)

app.use(cookieParser())
app.use(passport.initialize())
app.use(passport.session())

passport.serializeUser((user, done) => {
    console.log("serialize user")
    done(null, user.id);
});

passport.deserializeUser((id, done) => {

    // procurar user pelo id
    //done(null, user);
    console.log("deserialize id")
    done(null, id);

});

var CurrentUser;

passport.use(new TwitterStrategy({
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    callbackURL: process.env.CALLBACK_URL
  },
  async function(token, tokenSecret, profile, done) {
      console.log('logando usuario - ' + profile._json.id_str)

      CurrentUser = await Usuarios.findOne(
          { where: {
            id_twitter: profile._json.id_str
          }
        });

      if (!CurrentUser) {
        const newUser =  await Usuarios.create({
            nome: profile._json.name,
            id_twitter: profile._json.id_str,
            url_foto: profile._json.profile_image_url,
            javotou: 0
        })

        if (newUser) {
            CurrentUser = newUser
            done(null, newUser);
        }

      }
      console.log('currentUser')
      done(null, CurrentUser);

  }
));



const authCheck = (req, res, next) => {
    if (!req.user) {
        res.render('login')
    } else {
      next();
    }
}

app.get('/', function(req, res) { 

    const template = 'home'
    res.render(template, {
        user: req.user
    })
})

app.get('/ranking',authCheck , async function(req, res) { 

    const template = 'ranking'

    res.render(template, {
        user: req.user
    })
})

app.get('/voto', authCheck, async function(req, res) { 

    //se ja votou, manda pro /ranking

    const template = 'voto'
    console.log("voto --------------")
    console.log(req.user)



    console.log(CurrentUser)

    res.render(template, {
        user: CurrentUser.nome,
        javotou: CurrentUser.javotou
    })

})

app.get('/salvarvoto', function(req, res){
    console.log(req.url)

    //salva no DB
    res.render('ranking', {
        user: req.user
    })
})



app.get("/auth/logout", (req, res) => {
    req.logout();
    res.redirect("/");
})

app.get('/auth/twitter', passport.authenticate('twitter'));

app.get('/auth/twitter/callback', 
  passport.authenticate('twitter', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    console.log("callback")
    res.redirect('/voto');
})

const port = process.env.PORT
app.listen(port, function() {
      console.log(`Example app listening at http://localhost:${port}`)
})